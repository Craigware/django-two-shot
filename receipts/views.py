from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


def ReceiptList(request):
    receipt_list = None
    if request.user.is_authenticated:
        receipt_list = Receipt.objects.filter(purchaser=request.user)
    else:
        return redirect("login")

    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "home.html", context)


def AccountList(request):
    account_list = None
    if request.user.is_authenticated:
        account_list = Account.objects.filter(owner=request.user)
    else:
        return redirect("login")

    context = {
        "account_list": account_list,
    }
    return render(request, "accounts.html", context)


def CategoryList(request):
    category_list = None
    if request.user.is_authenticated:
        category_list = ExpenseCategory.objects.filter(owner=request.user)
    else:
        return redirect("login")

    context = {
        "category_list": category_list,
    }
    return render(request, "categories.html", context)


def CreateReceipt(request):
    if request.user.is_authenticated is False:
        return redirect("home")
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            vendor = form.cleaned_data["vendor"]
            tax = form.cleaned_data["tax"]
            total = form.cleaned_data["total"]
            date = form.cleaned_data["date"]
            category = form.cleaned_data["category"]
            account = form.cleaned_data["account"]
            Receipt.objects.create(
                vendor=vendor,
                tax=tax,
                total=total,
                date=date,
                purchaser=request.user,
                category=category,
                account=account,
            )
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "create_receipt.html", context)


def CreateCategory(request):
    if request.user.is_authenticated is False:
        return redirect("home")
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            owner = request.user

            ExpenseCategory.objects.create(
                name=name,
                owner=owner,
            )

            return redirect("category_list")
    else:
        form = CategoryForm()

    context = {"form": form}
    return render(request, "create_category.html", context)


def CreateAccount(request):
    if request.user.is_authenticated is False:
        return redirect("home")
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data["name"]
            number = form.cleaned_data["number"]
            owner = request.user

            Account.objects.create(
                name=name,
                number=number,
                owner=owner,
            )

            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}
    return render(request, "create_account.html", context)
